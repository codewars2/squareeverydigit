﻿using System.Collections.Generic;
using System.Text;

namespace SquareEveryDigit.Algorithms
{
    class DigitsAlgorithmService
    {
        public static int SquareDigits(int n)
        {
            List<int> numList = new List<int>();
            int digit;
            do
            {
                digit = n % 10;
                n /= 10;
                numList.Insert(0, digit * digit);
            } while (n != 0);

            return int.Parse(string.Join("", numList.ToArray()));
        }

        public static int SquareDigitsStringEdition(int n)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string nString = n.ToString();

            foreach (var numString in nString)
            {
                int x = int.Parse(numString.ToString());
                stringBuilder.Append((x * x).ToString());
            }

            return int.Parse(stringBuilder.ToString());
        }
    }
}
