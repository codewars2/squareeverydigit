﻿using SquareEveryDigit.Algorithms;
using System;

namespace SquareEveryDigit
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                foreach (var item in args)
                {
                    var square = DigitsAlgorithmService.SquareDigits(int.Parse(item));
                    Console.WriteLine(square);
                }
            }
            else
            {
                Console.WriteLine("Enter a number:");
                string stringNumber = Console.ReadLine();
                var square = DigitsAlgorithmService.SquareDigits(int.Parse(stringNumber));
                Console.WriteLine(square);
            }
        }
    }
}
